---
layout: archive
title: "CV"
permalink: /cv/
author_profile: true
redirect_from:
  - /resume
---

{% include base_path %}

## Contact Information

Email:xiayutong618@gmail.com

## Education
* Ph.D. in *Data Science*, NUS Graduate School (NGS), National University of Singapore (NUS), 2022-Present
* M.S. in *Smart Cities and Urban Analytics*, The Bartlett Centre for Advanced Spatial Analysis (CASA), University College London (UCL), 2020-2021
* B.S. in *Land Resource Management*, College of Public Administration, Huazhong Agricultural University (HZAU), 2016-2020
* B.A. in *English*, School of Foreign Languages, Huazhong University of Science and Technology (HUST), 2018-2020


## Publication


## Skills
* _Technical_: QGIS, Cesium, SAS, SQL, LaTeX, HTML.
  - Python (numpy, pandas, scikit-learn, matplotlib, Geopandas, networkx, tensorflow)
  - R (tidyverse, ggplot2)
* _Spatial Analysis_: GWR, Network Analysis, Accessibility.
* _Machine Learning_: Classification, Regression, Clustering, Neural Networks.

## Research and Competition Experience
### Research

* MS Dissertation: A Random Effect Bayesian Neural Network (RE-BNN) for Choice Analysis: Predicting Travel ModeChoice Across Multiple Cities (May. 2021-Present)
  - Mentor: Dr. Huanfa Chen 
  - Under the utility theory, we combined Random Effects models and Bayesian Neural Networks to develop a new framework for travel mode choice prediction across multiple cities. Via exploring the offset utility parameters, choice probability functions, and travel mode market shares derived from the model, we demonstrated its capability of learning the regional heterogeneity of travel behaviours.

* BS Dissertation: The Impact of Traffic on Equality of Urban Healthcare Service Accessibility (May. 2020)
  - Mentor: Prof. Chengchao Zuo
  - This paper measured dynamic accessibility to healthcare in the study area using two methods: Gaussian Two-Step Floating Catchment Area (G2SFCA) and Weighted Average Travel Time (WATT). Then, it investigated how traffic conditions affect the spatial and demographic equality of healthcare services at different times.
  - We found that traffic conditions change the accessibility by extending travel time andreducing the likelihood of obtaining healthcare services during peak hours, especiallyfor suburban residents. Regarding equality evaluation, the impact of traffic variability on spatial equality is much less significant than that on demographical equality.


* Impact of Coupling Effect of Urban Expansion and Cultivated Land Protection on Ecosystem NPP (Jan. 2018–May. 2019)
  - Research Member
  - Mentor: Prof. Xinli Ke
  - Responsible for land-use change analysis with ArcGIS and NPP calculation.
  - Processed Year 2000 and 2010 LUCC data of Hubei Province, obtained LUCC data of one-time occupation of cultivated land and ecological land by urban expansion and the secondary occupation of ecological land by cultivated land protection policy.
  - Employed Carnegie-Ames-Stanford Approach to calculate the direct impact or urban expansion on NPP and indirect impact of cultivated land protection system on NPP, based on Total radiation, NDVI and Temperature and water scale.
 
### Competition
* [CUSP London Data Dive 2021 - Future Mobility in Cities](https://cusplondon.ac.uk/events.html) (15-19th Mar. 2021)
  - Team member
  - Analysing the potential for micro-mobility and e-bikes to transform urban mobility in UK cities. 
  - Responsible for using data exploration and random forest with Python, based on the data on shared cycling, weather and air quality in London in 2019, to analyse the influencing factors of bike usage.

* Urban Land Grading Grading in the Pearl River Delta (May. 2019–Aug. 2019)
  - Group Member
  - Mentor: Prof. Chen Zeng
  - Responsible for land grading in Zhongshan City based on the comprehensive influence of five indicators (commercial service prosperity, road accessibility, air quality, schools and water system) with ArcGIS.
  - Won the 3rd Prize in the 3rd Huazhong Agricultural University Geographic Information System Skills Competition.

## Internship
* Hubei Planning Survey Design Institute of Water Resources (Jul. 2020–Sept. 2020)
  - *Smart City Modelling Engineer (Intern)*
  - Captured building location data of Qianjiang in Google Earth
  - Modelled 3D buildings, roads and rivers of Qianjiang with Cesium

* Huaxia Huanyu Survey and Evaluation Co., Ltd. (Jul. 2019–Sept. 2019)
  - *Permanent Basic Farmland Designation Staff (Intern)*
  - Involved in The third China National Land Survey
  - Responsible for municipal level demonstration and inspection works
  - Distinguished basic farmland reserve area via remote sensing images and photos

## Awards
- Outstanding Graduate at HZAU (Jun. 2020)
- Three-Good student at HZAU (May. 2020)
- First Prize for Excellent Academic Performance at HZAU (Oct.2018)

## Language
- Chinese
- English (IELTS:7)
