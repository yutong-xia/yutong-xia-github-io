---
permalink: /
title: "About Me"
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---
I am Xia Yutong, a Ph.D. student in Data Science at [NUS Graduate School (NGS)](https://nusgs.nus.edu.sg/), National University of Singapore. I obtained my master's degree in Smart Cities and Urban Analytics from [the Bartlett Centre for Advanced Spatial Analysis (CASA)](https://www.ucl.ac.uk/bartlett/casa), University College London in Dec. 2021. Before that, I obtained my bachelor degree in Land Resource Management from [the College of Public Administration](https://ggxy.hzau.edu.cn/), Huazhong Agricultural University in Jun. 2020. I am interested in urban data analysis and urban mobility modelling. You can reach me by email: xiayutong618@gmail.com. 

More information about me can be found in the [CV page](https://yutong-xia.github.io/cv/).



## News






